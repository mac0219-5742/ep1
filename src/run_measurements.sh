#! /bin/bash

set -o xtrace

MEASUREMENTS=10
SIZE_ITERATIONS=10
THREAD_ITERATIONS=6
INITIAL_SIZE=16
INITIAL_THREAD_NUM=1

NAMES=('mandelbrot_seq' 'mandelbrot_pth' 'mandelbrot_omp')

make
mkdir results

for NAME in ${NAMES[@]}; do
    mkdir results/$NAME
    
    SIZE=$INITIAL_SIZE

    for ((i=1; i<=$SIZE_ITERATIONS; i++)); do
        
        THREAD_NUM=$INITIAL_THREAD_NUM
        
        for ((j=1; j<=$THREAD_ITERATIONS; j++)); do
            
            perf stat -r $MEASUREMENTS ./$NAME -2.5 1.5 -2.0 2.0 $SIZE $THREAD_NUM >> full.log 2>&1
            perf stat -r $MEASUREMENTS ./$NAME -0.8 -0.7 0.05 0.15 $SIZE $THREAD_NUM >> seahorse.log 2>&1
            perf stat -r $MEASUREMENTS ./$NAME 0.175 0.375 -0.1 0.1 $SIZE $THREAD_NUM >> elephant.log 2>&1
            perf stat -r $MEASUREMENTS ./$NAME -0.188 -0.012 0.554 0.754 $SIZE $THREAD_NUM >> triple_spiral.log 2>&1

            THREAD_NUM=$(($THREAD_NUM * 2))
        done
        
        SIZE=$(($SIZE * 2))
    done

    mv *.log results/$NAME
    rm output.ppm
done
